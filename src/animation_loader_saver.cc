#include "bone_geometry.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <initializer_list>
#include <utility>
#include <set>
#include <map>
#include "json.hpp"

/*
 * Placeholder functions for Milestone 2
 */

void Mesh::saveAnimationTo(const std::string& fn)
{
    using json = nlohmann::json;
    json j;
    std::vector<std::map<std::string, std::vector<double>>> frames;
    
    for (int i = 0; i < keyframes.size(); i++) {
        std::map<std::string, std::vector<double>> kf;
        KeyFrame k = keyframes.at(i);
        if (k.world_pos != glm::vec3(0,0,0)) {
            std::string first = "-1";
            std::vector<double> second = std::vector<double>({k.world_pos.x, k.world_pos.y, k.world_pos.z});
            kf.insert({first, second});
        }
        for (int k = 0; k < keyframes.at(i).rel_rot.size(); k++) {
            glm::fquat r = keyframes.at(i).rel_rot.at(k);
            if (r != glm::fquat(1.0, 0.0, 0.0, 0.0)) {
                std::pair<std::string, std::vector<double>> rot;
                std::string first = std::to_string(k);
                std::vector<double> second = std::vector<double>({r.x, r.y, r.z, r.w});
                kf.insert({first, second});
            }    
        }
        frames.push_back(kf);
    }

    j["frames"] = frames;
    std::ofstream file(fn);
    file << j.dump() << std::endl;
}

void Mesh::loadAnimationFrom(const std::string& fn)
{
    using json = nlohmann::json;

    std::ifstream myfile;
    myfile.open(fn);
    if (!myfile) return;
    int num_children;
    Joint& wj = skeleton.world_joint;
    std::string line;
    getline(myfile, line);
    std::istringstream iss = std::istringstream(line);
    iss >> wj.joint_index >> wj.parent_index >> wj.position.x >> wj.position.y >> wj.position.z >> wj.orientation.x >> wj.orientation.y >>
        wj.orientation.z >> wj.orientation.w >> wj.rel_orientation.x >> wj.rel_orientation.y >> wj.rel_orientation.z >> wj.rel_orientation. w >>
        wj.init_position.x >> wj.init_position.y >> wj.init_position.z >> wj.init_rel_position.x >> wj.init_rel_position.y >> wj.init_rel_position.z >>
        wj.rel_position.x >> wj.rel_position.y >> wj.rel_position.z >> wj.w.x >> wj.w.y >> wj.w.z >> num_children;
    for (int i = 0; i < num_children; i++) {
        int child;
        myfile >> child;
        wj.children.emplace_back(child);
    }
    getline(myfile, line);
    iss = std::istringstream(line);
    int num_joints;
    iss >> num_joints;
    for (int i = 0; i < num_joints; i++) {
        std::cerr << i << std::endl;
        getline(myfile, line);
        std::cerr << line << std::endl;
        iss = std::istringstream(line);
        Joint& wj = skeleton.joints.at(i);
        iss >> wj.joint_index >> wj.parent_index >> wj.position.x >> wj.position.y >> wj.position.z >> wj.orientation.x >> wj.orientation.y >>
            wj.orientation.z >> wj.orientation.w >> wj.rel_orientation.x >> wj.rel_orientation.y >> wj.rel_orientation.z >> wj.rel_orientation. w >>
            wj.init_position.x >> wj.init_position.y >> wj.init_position.z >> wj.init_rel_position.x >> wj.init_rel_position.y >> wj.init_rel_position.z >>
            wj.rel_position.x >> wj.rel_position.y >> wj.rel_position.z >> wj.w.x >> wj.w.y >> wj.w.z >> num_children;
        for (int i = 0; i < num_children; i++) {
            int child;
            myfile >> child;
            wj.children.emplace_back(child);
        }   
    }
    std::cerr << "after loop" << std::endl;
    getline(myfile, line);
    iss = std::istringstream(line);
    int num_bones;
    iss >> num_bones;
    std::cerr << num_bones << std::endl;
    std::cerr << line << std::endl;
    for (int i = 0; i < num_bones; i++) {
        getline(myfile, line);
        iss = std::istringstream(line);
        Bone& b = skeleton.bones.at(i);
        Cylinder& c = b.cylinder;
        int p_joint_id;
        int c_joint_id;
        iss >> b.bone_id >> p_joint_id >> c_joint_id >> b.p.x >> b.p.y >> b.p.z >> b.c.x >> b.c.y >> b.c.z >> b.v.x >> b.v.y >> b.v.z >> b.h >>
            c.p_.x >> c.p_.y >> c.p_.z >> c.c_.x >> c.c_.y >> c.c_.z >> c.v_.x >> c.v_.y >> c.v_.z >> c.n_.x >> c.n_.y >> c.n_.z >> c.b_.x >> c.b_.y >> c.b_.z >>
            c.r_.x >> c.r_.y >> c.r_.z >> c.r_.w >> c.h_;
        b.p_joint_ = skeleton.joints.at(p_joint_id);
        b.c_joint_ = skeleton.joints.at(c_joint_id);
    }


}