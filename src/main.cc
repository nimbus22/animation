#include <GL/glew.h>

#include "bone_geometry.h"
#include "procedure_geometry.h"
#include "render_pass.h"
#include "config.h"
#include "gui.h"
#include "texture_to_render.h"

#include <memory>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>

#include <glm/gtx/component_wise.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/io.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <debuggl.h>
// #include <ft2build.h>
// #include FT_FREETYPE_H

int window_width = 1280;
int window_height = 720;
int main_view_width = 960;
int main_view_height = 720;
int preview_width = window_width - main_view_width; // 320
int preview_height = preview_width / 4 * 3; // 320 / 4 * 3 = 240
int preview_bar_width = preview_width;
int preview_bar_height = main_view_height;
const std::string window_title = "Animation";

const char* vertex_shader =
#include "shaders/default.vert"
;

const char* blending_shader =
#include "shaders/blending.vert"
;

const char* geometry_shader =
#include "shaders/default.geom"
;

const char* fragment_shader =
#include "shaders/default.frag"
;

const char* floor_fragment_shader =
#include "shaders/floor.frag"
;

const char* bone_vertex_shader =
#include "shaders/bone.vert"
;

const char* bone_fragment_shader =
#include "shaders/bone.frag"
;

// FIXME: Add more shaders here.

const char* cylinder_vertex_shader =
#include "shaders/cylinder.vert"
;

const char* cylinder_fragment_shader =
#include "shaders/cylinder.frag"
;

const char* axes_vertex_shader =
#include "shaders/axes.vert"
;

const char* axes_fragment_shader =
#include "shaders/axes.frag"
;

const char* preview_vertex_shader =
#include "shaders/preview.vert"
;

const char* preview_fragment_shader = 
#include "shaders/preview.frag"
;

// const char* text_vertex_shader = 
// #include "shaders/text.vert"
// ;

// const char* text_fragment_shader =
// #include "shaders/text.frag"
// ;

const char* text_box_vertex_shader =
#include "shaders/textbox.vert"
;

const char* text_box_fragment_shader =
#include "shaders/textbox.frag"
;

const char* input_box_vertex_shader =
#include "shaders/inputbox.vert"
;

const char* input_box_fragment_shader =
#include "shaders/inputbox.frag"
;

unsigned compileShader(const char* source_ptr, int type)
{
	GLuint ret = 0;
	CHECK_GL_ERROR(ret = glCreateShader(type));
#if 0
	std::cerr << __func__ << " shader id " << ret << " type " << type << "\tsource:\n" << source_ptr << std::endl;
#endif
	CHECK_GL_ERROR(glShaderSource(ret, 1, &source_ptr, nullptr));
	glCompileShader(ret);
	CHECK_GL_SHADER_ERROR(ret);
	return ret;
}

void ErrorCallback(int error, const char* description) {
	std::cerr << "GLFW Error: " << description << "\n";
}

struct Character {
    GLuint     TextureID;  // ID handle of the glyph texture
    glm::ivec2 Size;       // Size of glyph
    glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
    GLuint     Advance;    // Offset to advance to next glyph
};

GLFWwindow* init_glefw()
{
	if (!glfwInit())
		exit(EXIT_FAILURE);
	glfwSetErrorCallback(ErrorCallback);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); // Disable resizing, for simplicity
	glfwWindowHint(GLFW_SAMPLES, 4);
	// glEnable(GL_MULTISAMPLE);  
	auto ret = glfwCreateWindow(window_width, window_height, window_title.data(), nullptr, nullptr);
	CHECK_SUCCESS(ret != nullptr);
	glfwMakeContextCurrent(ret);
	glewExperimental = GL_TRUE;
	CHECK_SUCCESS(glewInit() == GLEW_OK);
	glGetError();  // clear GLEW's error for it
	glfwSwapInterval(1);
	const GLubyte* renderer = glGetString(GL_RENDERER);  // get renderer string
	const GLubyte* version = glGetString(GL_VERSION);    // version as a string
	std::cout << "Renderer: " << renderer << "\n";
	std::cout << "OpenGL version supported:" << version << "\n";

	return ret;
}

void RenderText(GLuint &s, std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color, GLuint& VAO, GLuint& VBO, std::map<GLchar, Character>& Characters)
{
    // Activate corresponding render state	
    // s.Use();
    glUniform3f(glGetUniformLocation(s, "textColor"), color.x, color.y, color.z);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(VAO);

    // Iterate through all characters
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        Character ch = Characters[*c];

        GLfloat xpos = x + ch.Bearing.x * scale;
        GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

        GLfloat w = ch.Size.x * scale;
        GLfloat h = ch.Size.y * scale;
        // Update VBO for each character
        GLfloat vertices[6][4] = {
            { xpos,     ypos + h,   0.0, 0.0 },            
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }           
        };
        // Render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        // Update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); 
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}


int main(int argc, char* argv[])
{
	if (argc < 2) {
		std::cerr << "Input model file is missing" << std::endl;
		std::cerr << "Usage: " << argv[0] << " <PMD file>" << std::endl;
		return -1;
	}
	GLFWwindow *window = init_glefw();
	GUI gui(window, main_view_width, main_view_height, preview_height);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glm::mat4 text_projection = glm::ortho(0.0f, (window_width * 1.0f), 0.0f, (window_height*1.0f));

	std::function<glm::mat4()> text_proj_data = [&text_projection]() { return text_projection; };
	auto text_proj = make_uniform("text_projection", text_proj_data);

	

	std::vector<glm::vec4> floor_vertices;
	std::vector<glm::uvec3> floor_faces;
	create_floor(floor_vertices, floor_faces);

	std::vector<glm::vec4> text_box_vertices;
	std::vector<glm::uvec3> text_box_faces;
	std::vector<glm::vec4> input_box_vertices;
	std::vector<glm::uvec3> input_box_faces;
	create_text_box(text_box_vertices, text_box_faces, input_box_vertices, input_box_faces);

	std::vector<glm::vec4> texture_quad_vertices;
	std::vector<glm::uvec3> texture_quad_faces;
	create_texture_quad(texture_quad_vertices, texture_quad_faces);


	LineMesh cylinder_mesh;
	LineMesh axes_mesh;

	// FIXME: we already created meshes for cylinders. Use them to render
	//        the cylinder and axes if required by the assignment.
	create_cylinder_mesh(cylinder_mesh);
	create_axes_mesh(axes_mesh);

	Mesh mesh;
	mesh.loadPmd(argv[1]);
	mesh.gui = &gui;
	std::cout << "Loaded object  with  " << mesh.vertices.size()
		<< " vertices and " << mesh.faces.size() << " faces.\n";

	glm::vec4 mesh_center = glm::vec4(0.0f);
	for (size_t i = 0; i < mesh.vertices.size(); ++i) {
		mesh_center += mesh.vertices[i];
	}
	mesh_center /= mesh.vertices.size();

	/*
	 * GUI object needs the mesh object for bone manipulation.
	 */
	gui.assignMesh(&mesh);

	glm::vec4 light_position = glm::vec4(0.0f, 100.0f, 0.0f, 1.0f);
	MatrixPointers mats; // Define MatrixPointers here for lambda to capture
	/*
	 * In the following we are going to define several lambda functions as
	 * the data source of GLSL uniforms
	 *
	 * Introduction about lambda functions:
	 *      http://en.cppreference.com/w/cpp/language/lambda
	 *      http://www.stroustrup.com/C++11FAQ.html#lambda
	 *
	 * Note: lambda expressions cannot be converted to std::function directly
	 *       Hence we need to declare the data function explicitly.
	 *
	 * CAVEAT: DO NOT RETURN const T&, which compiles but causes
	 *         segfaults.
	 *
	 * Do not worry about the efficient issue, copy elision in C++ 17 will
	 * minimize the performance impact.
	 *
	 * More details about copy elision:
	 *      https://en.cppreference.com/w/cpp/language/copy_elision
	 */

	// FIXME: add more lambdas for data_source if you want to use RenderPass.
	//        Otherwise, do whatever you like here
	std::function<const glm::mat4*()> model_data = [&mats]() {
		return mats.model;
	};
	std::function<glm::mat4()> view_data = [&mats]() { return *mats.view; };
	std::function<glm::mat4()> proj_data = [&mats]() { return *mats.projection; };
	std::function<glm::mat4()> identity_mat = [](){ return glm::mat4(1.0f); };
	std::function<glm::vec3()> cam_data = [&gui](){ return gui.getCamera(); };
	std::function<glm::vec4()> lp_data = [&light_position]() { return light_position; };

	auto std_model = std::make_shared<ShaderUniform<const glm::mat4*>>("model", model_data);
	auto floor_model = make_uniform("model", identity_mat);
	auto std_view = make_uniform("view", view_data);
	auto std_camera = make_uniform("camera_position", cam_data);
	auto std_proj = make_uniform("projection", proj_data);
	auto std_light = make_uniform("light_position", lp_data);

	std::function<float()> alpha_data = [&gui]() {
		static const float transparet = 0.5; // Alpha constant goes here
		static const float non_transparet = 1.0;
		if (gui.isTransparent())
			return transparet;
		else
			return non_transparet;
	};
	auto object_alpha = make_uniform("alpha", alpha_data);

	std::function<std::vector<glm::vec3>()> trans_data = [&mesh](){ return mesh.getCurrentQ()->transData(); };
	std::function<std::vector<glm::fquat>()> rot_data = [&mesh](){ return mesh.getCurrentQ()->rotData(); };
	auto joint_trans = make_uniform("joint_trans", trans_data);
	auto joint_rot = make_uniform("joint_rot", rot_data);
	// FIXME: define more ShaderUniforms for RenderPass if you want to use it.
	//        Otherwise, do whatever you like here
	std::function<std::vector<glm::mat4>()> tx_d_data = [&mesh](){ return mesh.getCurrentT(); };
	auto tx_d = make_uniform("tx_d", tx_d_data);
	std::function<std::vector<glm::mat4>()> tx_u_data = [&mesh](){ return mesh.getCurrentU(); };
	auto tx_u = make_uniform("tx_u", tx_u_data);

	// Floor render pass
	RenderDataInput floor_pass_input;
	floor_pass_input.assign(0, "vertex_position", floor_vertices.data(), floor_vertices.size(), 4, GL_FLOAT);
	floor_pass_input.assignIndex(floor_faces.data(), floor_faces.size(), 3);
	RenderPass floor_pass(-1,
			floor_pass_input,
			{ vertex_shader, geometry_shader, floor_fragment_shader},
			{ floor_model, std_view, std_proj, std_light },
			{ "fragment_color" }
			);

	// PMD Model render pass
	// FIXME: initialize the input data at Mesh::loadPmd
	std::vector<glm::vec2>& uv_coordinates = mesh.uv_coordinates;
	RenderDataInput object_pass_input;
	object_pass_input.assign(0, "jid0", mesh.joint0.data(), mesh.joint0.size(), 1, GL_INT);
	object_pass_input.assign(1, "jid1", mesh.joint1.data(), mesh.joint1.size(), 1, GL_INT);
	object_pass_input.assign(2, "w0", mesh.weight_for_joint0.data(), mesh.weight_for_joint0.size(), 1, GL_FLOAT);
	object_pass_input.assign(3, "vector_from_joint0", mesh.vector_from_joint0.data(), mesh.vector_from_joint0.size(), 3, GL_FLOAT);
	object_pass_input.assign(4, "vector_from_joint1", mesh.vector_from_joint1.data(), mesh.vector_from_joint1.size(), 3, GL_FLOAT);
	object_pass_input.assign(5, "normal", mesh.vertex_normals.data(), mesh.vertex_normals.size(), 4, GL_FLOAT);
	object_pass_input.assign(6, "uv", uv_coordinates.data(), uv_coordinates.size(), 2, GL_FLOAT);
	// TIPS: You won't need vertex position in your solution.
	//       This only serves the stub shader.
	object_pass_input.assign(7, "vert", mesh.vertices.data(), mesh.vertices.size(), 4, GL_FLOAT);
	object_pass_input.assignIndex(mesh.faces.data(), mesh.faces.size(), 3);
	object_pass_input.useMaterials(mesh.materials);
	RenderPass object_pass(-1,
			object_pass_input,
			{
			  blending_shader,
			  geometry_shader,
			  fragment_shader
			},
			{ std_model, std_view, std_proj,
			  std_light,
			  std_camera, object_alpha,
			  joint_trans, joint_rot, tx_d, tx_u,
			},
			{ "fragment_color" }
			);

	// Setup the render pass for drawing bones
	// FIXME: You won't see the bones until Skeleton::joints were properly
	//        initialized
	std::vector<int> bone_vertex_id;
	std::vector<glm::uvec2> bone_indices;
	for (int i = 0; i < (int)mesh.skeleton.joints.size(); i++) {
		bone_vertex_id.emplace_back(i);
	}
	for (const auto& joint: mesh.skeleton.joints) {
		if (joint.parent_index < 0)
			continue;
		bone_indices.emplace_back(joint.joint_index, joint.parent_index);
	}
	RenderDataInput bone_pass_input;
	bone_pass_input.assign(0, "jid", bone_vertex_id.data(), bone_vertex_id.size(), 1, GL_UNSIGNED_INT);
	bone_pass_input.assignIndex(bone_indices.data(), bone_indices.size(), 2);
	RenderPass bone_pass(-1, bone_pass_input,
			{ bone_vertex_shader, nullptr, bone_fragment_shader},
			{ std_model, std_view, std_proj, joint_trans },
			{ "fragment_color" }
			);

	// FIXME: Create the RenderPass objects for bones here.
	//        or do whatever you like.
	std::function<glm::mat4()> bone_transform_matrix = [&gui, &mesh]() { 
		int current_bone = gui.getCurrentBone();
		if (current_bone != -1) return mesh.skeleton.bones.at(current_bone).cylinder.bone_transform();
		else return glm::mat4(0.0f);};
	auto bone_transform = make_uniform("bone_transform", bone_transform_matrix);


	RenderDataInput cylinder_pass_input;
	cylinder_pass_input.assign(0, "vertex_position", cylinder_mesh.vertices.data(), cylinder_mesh.vertices.size(), 4, GL_FLOAT);
	cylinder_pass_input.assignIndex(cylinder_mesh.indices.data(), cylinder_mesh.indices.size(), 2);
	RenderPass cylinder_pass(-1, cylinder_pass_input,
			{ cylinder_vertex_shader, nullptr, cylinder_fragment_shader},
			{ bone_transform, std_model, std_view, std_proj},
			{ "fragment_color" }
			);

	RenderDataInput axes_pass_input;
	axes_pass_input.assign(0, "vertex_position", axes_mesh.vertices.data(), axes_mesh.vertices.size(), 4, GL_FLOAT);
	axes_pass_input.assignIndex(axes_mesh.indices.data(), axes_mesh.indices.size(), 2);
	RenderPass axes_pass(-1, axes_pass_input,
			{ axes_vertex_shader, nullptr, axes_fragment_shader},
			{ bone_transform, std_model, std_view, std_proj},
			{ "fragment_color" }
			);

	// RenderDataInput text_box_pass_input;
	// text_box_pass_input.assign(0, "vertex_position", text_box_vertices.data(), text_box_vertices.size(), 4, GL_FLOAT);
	// text_box_pass_input.assignIndex(text_box_faces.data(), text_box_faces.size(), 3);
	// RenderPass text_box_pass(-1, text_box_pass_input,
	// 		{ text_box_vertex_shader, nullptr, text_box_fragment_shader},
	// 		{ text_proj},
	// 		{ "fragment_color" }
	// 		);

	// RenderDataInput input_box_pass_input;
	// input_box_pass_input.assign(0, "vertex_position", input_box_vertices.data(), input_box_vertices.size(), 4, GL_FLOAT);
	// input_box_pass_input.assignIndex(input_box_faces.data(), input_box_faces.size(), 3);
	// RenderPass input_box_pass(-1, input_box_pass_input,
	// 		{ input_box_vertex_shader, nullptr, input_box_fragment_shader},
	// 		{ text_proj},
	// 		{ "fragment_color" }
	// 		);

	// RenderDataInput text_pass_input;
	// text_pass_input.assign(0, "vertex", )


	// ================================================================
	// CREATE FRAMEBUFFER FOR PREVIEWS
	// ================================================================
	// // The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	// GLuint FramebufferName = 0;
	// glGenFramebuffers(1, &FramebufferName);
	// glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// // The texture we're going to render to
	// GLuint renderedTexture;
	// glGenTextures(1, &renderedTexture);

	// // "Bind" the newly created texture : all future texture functions will modify this texture
	// glBindTexture(GL_TEXTURE_2D, renderedTexture);

	// // Give an empty image to OpenGL ( the last "0" )
	// glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, main_view_width, main_view_height, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);

	// // Poor filtering. Needed !
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// // The depth buffer
	// GLuint depthrenderbuffer;
	// glGenRenderbuffers(1, &depthrenderbuffer);
	// glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	// glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, main_view_width, main_view_height);
	// glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	// // Set "renderedTexture" as our colour attachement #0
	// glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

	// // Set the list of draw buffers.
	// GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	// glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

	// // Always check that our framebuffer is ok
	// if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	// return false;

	// // Render to our framebuffer
	// glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
	// glViewport(0,0,main_view_width,main_view_height); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	// static const GLfloat g_quad_vertex_buffer_data[] = {
	// 	-1.0f, -1.0f, 0.0f,
	// 	1.0f, -1.0f, 0.0f,
	// 	-1.0f,  1.0f, 0.0f,
	// 	-1.0f,  1.0f, 0.0f,
	// 	1.0f, -1.0f, 0.0f,
	// 	1.0f,  1.0f, 0.0f,
	// };
	// RenderDataInput texture_quad_pass_input;
	// texture_quad_pass_input.assign(0, "vertex_position", texture_quad_vertices.data(), texture_quad_vertices.size(), 4, GL_FLOAT);
	// texture_quad_pass_input.assignIndex(texture_quad_faces.data(), texture_quad_faces.size(), 3);
	// RenderPass texture_quad_pass(-1, texture_quad_pass_input,
	// 		{ preview_vertex_shader, nullptr, preview_fragment_shader},
	// 		{ text_proj},
	// 		{ "fragment_color" }
	// 		);
	// // Generate FrameBuffer
	// GLuint FramebufferName = 0;
	// glGenFramebuffers(1, &FramebufferName);
	// // Generate Texture
	// GLuint renderedTexture;
	// glGenTextures(1, &renderedTexture);
	// // Generate Depth Buffer
	// GLuint depthrenderbuffer;
	// glGenRenderbuffers(1, &depthrenderbuffer);
	// // Initialize Draw Buffer
	// GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};


	// ================================================================
	// END
	// ================================================================


	float aspect = 0.0f;
	std::cout << "center = " << mesh.getCenter() << "\n";

	bool draw_floor = true;
	bool draw_skeleton = true;
	bool draw_object = true;
	bool draw_cylinder = true;

	if (argc >= 3) {
		mesh.loadAnimationFrom(argv[2]);
	}

	while (!glfwWindowShouldClose(window)) {
		// Setup some basic window stuff.
		glfwGetFramebufferSize(window, &window_width, &window_height);
		glViewport(0, 0, main_view_width, main_view_height);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_MULTISAMPLE);
		glEnable(GL_BLEND);
		glEnable(GL_CULL_FACE);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDepthFunc(GL_LESS);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glCullFace(GL_BACK);

		gui.updateMatrices();
		mats = gui.getMatrixPointers();

		std::stringstream title;
		float cur_time = gui.getCurrentPlayTime();
		title << window_title;
		if (gui.isPlaying()) {
			title << " Playing: "
			      << std::setprecision(2)
			      << std::setfill('0') << std::setw(6)
			      << cur_time << " sec";
			
			mesh.updateAnimation(cur_time);
		} else if (gui.isPoseDirty()) {
			title << " Editing";
			mesh.updateAnimation();
			gui.clearPose();
		}
		else
		{
			title << " Paused: "
				<< std::setprecision(2)
				<< std::setfill('0') << std::setw(6)
				<< cur_time << " sec";
		}

		glfwSetWindowTitle(window, title.str().data());

		// FIXME: update the preview textures here

		int current_bone = gui.getCurrentBone();

		// Draw bones first.
		if (draw_skeleton && gui.isTransparent()) {
			bone_pass.setup();
			// Draw our lines.
			// FIXME: you need setup skeleton.joints properly in
			//        order to see the bones.
			CHECK_GL_ERROR(glDrawElements(GL_LINES,
			                              bone_indices.size() * 2,
			                              GL_UNSIGNED_INT, 0));
		}
		draw_cylinder = (current_bone != -1 && gui.isTransparent());
		if (draw_cylinder) {
			cylinder_pass.setup();
			// std::cerr << "Drawing cylinder" << std::endl;
			CHECK_GL_ERROR(glDrawElements(GL_LINES,
										  cylinder_mesh.indices.size() * 2,
										  GL_UNSIGNED_INT, 0));
			axes_pass.setup();
			CHECK_GL_ERROR(glDrawElements(GL_LINES,
										  axes_mesh.indices.size() * 2,
										  GL_UNSIGNED_INT, 0));
		}

		// Then draw floor.
		if (draw_floor) {
			floor_pass.setup();
			// Draw our triangles.
			CHECK_GL_ERROR(glDrawElements(GL_TRIANGLES,
			                              floor_faces.size() * 3,
			                              GL_UNSIGNED_INT, 0));
		}

		// Draw the model
		if (draw_object) {
			object_pass.setup();
			int mid = 0;
			while (object_pass.renderWithMaterial(mid))
				mid++;
#if 0
			// For debugging also
			if (mid == 0) // Fallback
				CHECK_GL_ERROR(glDrawElements(GL_TRIANGLES, mesh.faces.size() * 3, GL_UNSIGNED_INT, 0));
#endif
		}

		// if (gui.isSavingFile()) {
		// 	// std::cout << "saving file " << std::endl;
		// 	text_box_pass.setup();
		// 	CHECK_GL_ERROR(glDrawElements(GL_TRIANGLES,
		// 								  text_box_faces.size() * 3,
		// 								  GL_UNSIGNED_INT, 0));

		// 	input_box_pass.setup();
		// 	CHECK_GL_ERROR(glDrawElements(GL_TRIANGLES,
		// 								  input_box_faces.size() * 3,
		// 								  GL_UNSIGNED_INT, 0));

		// 	// ===========================================================================
		// 	// TEXT BASED RENDERING VAO AND VBO SETUP
		// 	// ===========================================================================
			
		// 	// glGenVertexArrays(1, &VAO);
		// 	// glGenBuffers(1, &VBO);
		// 	// glBindVertexArray(VAO);
		// 	// glBindBuffer(GL_ARRAY_BUFFER, VBO);
		// 	// glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
		// 	// glEnableVertexAttribArray(0);
		// 	// glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
		// 	// CHECK_GL_ERROR(glUseProgram(textshader));
		// 	// glUniformMatrix4fv(glGetUniformLocation(textshader, "text_projection"), 1, GL_FALSE, glm::value_ptr(text_projection));
		// 	// unsigned text_proj_uni_id;
		// 	// CHECK_GL_ERROR(text_proj_uni_id = glGetUniformLocation(textshader, text_proj->name.c_str()));
		// 	// text_proj->bind(text_proj_uni_id);
		// 	// glBindBuffer(GL_ARRAY_BUFFER, 0);
		// 	// glBindVertexArray(0);   

		// 	// // ===========================================================================
		// 	// // END
		// 	// // ===========================================================================
		// 	// glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		// 	// glClear(GL_COLOR_BUFFER_BIT);

		// 	// RenderText(textshader, "Input a filename to save keyframes. Press Enter to Save, or ESC to cancel.", 10.0f, 240.0f, 0.5f, glm::vec3(1.0, 1.0, 1.0), VAO, VBO, Characters);
		// 	// RenderText(textshader, "animation", 15.0f, 330.0f, 0.5f, glm::vec3(0.0, 0.0, 0.0), VAO, VBO, Characters);
			
		// }

		// FIXME: Draw previews here, note you need to call glViewport
		// if (gui.savingKeyframe()) {
		// 	std::cout << "saving keyframe" << std::endl;
		// 	// Bind the Framebuffer
		// 	texture_quad_pass.setup();
		// 	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
		// 	// "Bind" the newly created texture : all future texture functions will modify this texture
		// 	glBindTexture(GL_TEXTURE_2D, renderedTexture);

		// 	// Give an empty image to OpenGL ( the last "0" )
		// 	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, 1024, 768, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);

		// 	// Poor filtering. Needed !
		// 	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		// 	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			
		// 	// Bind the Depth Buffer
		// 	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
		// 	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1024, 768);
		// 	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

		// 	// // Set "renderedTexture" as our colour attachement #0
		// 	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);
		// 	glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

		// 	glBindFramebuffer(GL_FRAMEBUFFER, 0);
		// 	glClear(GL_DEPTH_BUFFER_BIT);
		// 	glViewport(0,0,1024,768);
		// 	// // Always check that our framebuffer is ok
		// 	// if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		// 	// return -1;

		// }







		// Poll and swap.
		glfwPollEvents();
		glfwSwapBuffers(window);
	}
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
