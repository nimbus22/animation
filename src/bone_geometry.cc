#include "config.h"
#include "bone_geometry.h"
#include "texture_to_render.h"
#include <fstream>
#include <queue>
#include <iostream>
#include <stdexcept>
#include <cassert>
#include <glm/gtx/io.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include "ray.h"

namespace {
	constexpr glm::fquat identity_quat(1.0, 0.0, 0.0, 0.0);
}

/*
 * For debugging purpose.
 */
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v) {
	size_t count = std::min(v.size(), static_cast<size_t>(10));
	for (size_t i = 0; i < count; ++i) os << i << " " << v[i] << "\n";
	os << "size = " << v.size() << "\n";
	return os;
}

std::ostream& operator<<(std::ostream& os, const BoundingBox& bounds)
{
	os << "min = " << bounds.min << " max = " << bounds.max;
	return os;
}

bool BoundingBox::intersect(const ray& r, float& tMin, float& tMax) const {
	glm::vec3 R0 = r.getPosition();
	glm::vec3 Rd = r.getDirection();
	tMin = -1.0e38;
	tMax = 1.0e38;
	float ttemp;
	for (int currentaxis = 0; currentaxis < 3; currentaxis++) {
		float vd = Rd[currentaxis];
		if (vd == 0.0)
			continue;
		float v1 = min[currentaxis] - R0[currentaxis];
		float v2 = max[currentaxis] - R0[currentaxis];
		float t1 = v1 / vd;
		float t2 = v2 / vd;
		if(t1 > t2) {
			ttemp = t1;
			t1 = t2;
			t2 = ttemp;
		}
		if (t1 > tMin)
			tMin = t1;
		if (t2 < tMax)
			tMax = t2;
		if (tMin > tMax)
			return false;
		if (tMax < RAY_EPSILON)
			return false;
	}
	return true;
}



const glm::vec3* Skeleton::collectJointTrans() const
{
	return cache.trans.data();
}

const glm::fquat* Skeleton::collectJointRot() const
{
	return cache.rot.data();
}

// FIXME: Implement bone animation.
bool Cylinder::intersect(const ray&r) const {
	ray temp = r;
	glm::mat4 translation_matrix = glm::mat4(1    , 0    , 0    , 0,
											 0    , 1    , 0    , 0,
											 0    , 0    , 1    , 0,
											 -p_.x, -p_.y, -p_.z, 1   );
	glm::mat4 rotation_matrix = glm::mat4(n_.x, b_.x, v_.x, 0,
										  n_.y, b_.y, v_.y, 0,
										  n_.z, b_.z, v_.z, 0,
										  0   , 0   , 0   , 1);
	glm::vec4 position = glm::vec4(r.getPosition(), 1);
	glm::vec4 direction = glm::vec4(r.getDirection(), 0);
	position = rotation_matrix * translation_matrix * position;
	direction = rotation_matrix * direction;
	float px = position.x;
	float py = position.y;
	float vx = direction.x;
	float vy = direction.y;
	float a = vx*vx + vy*vy;
	float b = 2*px*vx + 2*py*vy;
	float c = px*px + py*py - kCylinderRadius*kCylinderRadius;
	float disc = b*b - 4*a*c;
	if (disc < 0 || disc != disc) return false;
	float t1 = (-b + glm::sqrt(disc))/(2*a);
	float t2 = (-b - glm::sqrt(disc))/(2*a);
	float min;
	if (t1 < 0 && t2 < 0) return false;
	else if (t1 >= 0 && t2 >= 0) {
		min = glm::min(t1, t2);
	} else {
		min = glm::max(t1, t2);
	}
	if (min < 0) return false;
	glm::vec4 intersection = position + direction * min;
	if (intersection.z >= 0 && intersection.z <= h_) {
		// std::cerr << "min: " << min << " intersection height: " << intersection.z << " actual height: " << h_ << std::endl;
		return true;
	}
	return false;
}

glm::mat4 Cylinder::bone_transform() const {
	glm::mat4 extension_mat = glm::mat4(1, 0  , 0, 0,
										0, h_ , 0, 0,
										0, 0  , 1, 0,
										0, 0  , 0, 1);
	glm::mat4 rotation_mat = glm::mat4(n_.x, n_.y, n_.z, 0,
									   v_.x, v_.y, v_.z, 0,
									   b_.x, b_.y, b_.z, 0,
									   0   , 0   , 0   , 1);
	glm::mat4 translation_mat = glm::mat4(1   , 0   , 0   , 0,
										  0   , 1   , 0   , 0,
										  0   , 0   , 1   , 0,
										  p_.x, p_.y, p_.z, 1);
	// return translation_mat * rotation_mat * extension_mat;
	return translation_mat * rotation_mat * extension_mat;
}

bool Bone::intersect(const ray& r) const{
	float tMin;
	float tMax;
	//if (!bounds.intersect(r, tMin, tMax)) return false;
	// std::cerr << "intersect" << std::endl;
	if (!cylinder.intersect(r)) return false;
	// std::cerr << "cylinder" << std::endl;
	return true;
}

void Skeleton::refreshCache(Configuration* target)
{
	if (target == nullptr)
		target = &cache;
	target->rot.resize(joints.size());
	target->trans.resize(joints.size());
	for (size_t i = 0; i < joints.size(); i++) {
		target->rot[i] = joints[i].orientation;
		target->trans[i] = joints[i].position;
	}
}

int Skeleton::intersect(const ray& r) const {
	std::vector<Bone>::const_iterator begin = bones.begin();
	std::vector<Bone>::const_iterator end = bones.end();
	for (unsigned long i = 0; begin != end; ++i, ++begin) {
		Bone b = *begin;
		if (b.intersect(r)) {
			return i;
		}
	}
	return -1;
	// for (Bone b : bones) {
	// 	if (b.intersect(r)) {

	// 		return true;
	// 	}
	// }
	// return false;
}


Mesh::Mesh()
{
}

Mesh::~Mesh()
{
}

void Mesh::loadPmd(const std::string& fn)
{
	MMDReader mr;
	mr.open(fn);
	mr.getMesh(vertices, faces, vertex_normals, uv_coordinates);
	computeBounds();
	// bone_bounds.min = bounds.min;
	// bone_bounds.max = bounds.max;
	// std::cerr << vertices.size() << " " << faces.size() << " " << vertex_normals.size() << " " << uv_coordinates.size() << std::endl;
	mr.getMaterial(materials);

	// FIXME: load skeleton and blend weights from PMD file,
	//        initialize std::vectors for the vertex attributes,
	//        also initialize the skeleton as needed
	int id = 0;
	glm::vec3 wcoord;
	int parent;
	Joint new_joint;
	while (mr.getJoint(id, wcoord, parent)) {
		new_joint = Joint(id, wcoord, parent);
		// std::cerr << id << " " << wcoord.x << " " << wcoord.y << " " << wcoord.z << " " <<  parent << std::endl;

		if (parent != -1) {
			// Add the id of this joint to the parent joints list of children
			Joint& parent_joint = skeleton.joints.at(parent);
			parent_joint.children.push_back(id);
			
			// Create a bone that represents the bone connecting this joint to its parent joint.
			// Add this bone to the list of bones in our skeleton so that we can use it for Bone Manipulation
			Bone new_bone = Bone(parent_joint, new_joint, skeleton.bones.size());
			skeleton.bones.push_back(new_bone);
			// bone_bounds.min = glm::min(bone_bounds.min, new_bone.bounds.min);
			// bone_bounds.max = glm::max(bone_bounds.max, new_bone.bounds.max);



			// Initialize the relative position and the initial relative position as the position of this joint with the parent joint as the origin
			// Since the coordinate system of the joints are axis aligned with the world coordinates, the relative position retains the spatial
			// difference between the points corresponding to this joints position and the parent joints position
			new_joint.rel_position = new_joint.init_position - parent_joint.init_position;
			new_joint.init_rel_position = new_joint.rel_position;
		}
		else {
			// Add the id of this joint to the parent joints list of children
			// skeleton.world_joint = new_joint;
			if (id == 0) {
				Joint& parent_joint = skeleton.world_joint;
				Bone new_bone = Bone(parent_joint, new_joint, skeleton.bones.size());
				skeleton.bones.push_back(new_bone);
			} else
			{
				Joint& parent_joint = new_joint;
				Bone new_bone = Bone(parent_joint, new_joint, skeleton.bones.size());
				skeleton.bones.push_back(new_bone);
			}
			
			
			// parent_joint.children.push_back(id);
			
			
			// Initialize the relative position and the initial relative position as the position of this joint with the parent joint as the origin
			// Since the coordinate system of the joints are axis aligned with the world coordinates, the relative position retains the spatial
			// difference between the points corresponding to this joints position and the parent joints position
			new_joint.rel_position = new_joint.init_position - skeleton.world_joint.init_position;
			new_joint.init_rel_position = new_joint.rel_position;
		}
		// Now that we have edited created and set all the values for this joint, we can finally add it to the list of joints in our skeleton.
		skeleton.joints.push_back(new_joint);
		
		// Go to next joint id
		++id;
	}


	// Get and set all the joint weights and vertices datas that we need for skinning.
	std::vector<SparseTuple> tup;
	mr.getJointWeights(tup);
	for (int i = 0; i < tup.size(); i++) {
		
		SparseTuple& t = tup.at(i);
		// std::cerr << i << " " << t.vid << " " << t.jid0 << " " << t.jid1 << " " << t.weight0 << std::endl;
		joint0.push_back(t.jid0);
		joint1.push_back(t.jid1);
		glm::vec4 vertex = vertices.at(t.vid);
		glm::vec3 v = glm::vec3(vertex.x, vertex.y, vertex.z);
		int id0;
		glm::vec3 j0;
		int parent0;
		mr.getJoint(id0, j0, parent0);
		glm::vec3 vec0 = v - j0;
		vector_from_joint0.push_back(vec0);
		if (t.jid1 != -1) {
			Joint& j1 = skeleton.joints.at(t.jid1);
			glm::vec3 vec1 = v - j1.position;
			vector_from_joint1.push_back(vec1);
		}
		float weight = t.weight0;
		weight_for_joint0.push_back(weight);
	}
}

void Mesh::transform_vertex_weights() {
	for (int i = 0; i < vertices.size(); i++) {
		glm::vec4& vertex = vertices.at(i);
		Joint& j0 = skeleton.joints.at(joint0.at(i));
		Joint& j1 = skeleton.joints.at(joint1.at(i));
		glm::vec3 p0 = j0.position;
		glm::vec3 p1 = j1.position;
		glm::mat4 u0 = j0.u_i;
		glm::mat4 u1 = j1.u_i;
		glm::mat4 d0 = j0.get_di(skeleton.joints.at(j0.parent_index), skeleton.joints);
		glm::mat4 d1 = j1.get_di(skeleton.joints.at(j1.parent_index), skeleton.joints);
		glm::vec3 vj0 = vector_from_joint0.at(i);
		glm::vec3 vj1 = vector_from_joint1.at(i);
		float weight0 = weight_for_joint0.at(i);
		float weight1 = weight0 * glm::length(vj1) / glm::length(vj0); 
		glm::vec4 sum = glm::vec4(weight0 * d0 * glm::inverse(u0) * vertex) + glm::vec4(weight0 * d0 * glm::inverse(u0) * vertex);
	}
	
}

std::vector<glm::mat4> Mesh::getCurrentT() {
	std::vector<glm::mat4> result;
	for (int i = 0; i < skeleton.joints.size(); i++) {
		glm::mat4 tx = skeleton.getRecursiveD_i(skeleton.joints.at(i));
		result.push_back(tx);
	}
	return result;
}

std::vector<glm::mat4> Mesh::getCurrentU() {
	std::vector<glm::mat4> result;
	for (int i= 0; i < skeleton.joints.size(); i++) {
		glm::mat4 tx = skeleton.getRecursiveU_i(skeleton.joints.at(i));
		result.push_back(glm::inverse(tx));
	}
	return result;
}

int Mesh::getNumberOfBones() const
{
	return skeleton.joints.size();
}

void Mesh::computeBounds()
{
	bounds.min = glm::vec3(std::numeric_limits<float>::max());
	bounds.max = glm::vec3(-std::numeric_limits<float>::max());
	for (const auto& vert : vertices) {
		bounds.min = glm::min(glm::vec3(vert), bounds.min);
		bounds.max = glm::max(glm::vec3(vert), bounds.max);
	}
}

int Mesh::intersect(const ray& r) const {
	float tMin, tMax;
	// if (!bone_bounds.intersect(r, tMin, tMax)) {
	// // 	// std::cerr << "outside bone bounding box" << std::endl;
	// 	return -1;
	// }
	// std::cerr << "inside bone bounding box" << std::endl;
	return skeleton.intersect(r);
}

void Mesh::updateAnimation(float t)
{
	if (t != -1.0) {
		if (keyframes.size() > 1) {
			if (keyframe_index < keyframes.size() - 1) {
				if (t > time_to_current + next.time) {
					keyframe_index++;
					time_to_current += next.time;
				}
				current = keyframes.at(keyframe_index);
				if (keyframe_index == keyframes.size() - 1) {
					next = keyframes.at(keyframe_index);
				} else {
					next = keyframes.at(keyframe_index + 1);
				}
			} else {
				current = keyframes.at(keyframe_index);
				next = keyframes.at(keyframe_index);
			}
			float tau = current.time;
			tau = float((t*1.0f - time_to_current*1.0f)*1.0f) / float(tau*1.0f);
			assert (tau >= 0 && tau <= 1);
			KeyFrame temp;
			KeyFrame::interpolate(current, next, tau, temp);
			skeleton.world_joint.position = temp.world_pos;
			skeleton.world_joint.rel_position = temp.world_pos;
			for (int i = 0; i < temp.rel_rot.size(); i++) {
				skeleton.joints.at(i).rel_orientation = temp.rel_rot.at(i);
			}
		} 
	}
	
	gui->transformJoint(0);


	skeleton.refreshCache(&currentQ_);
	// FIXME: Support Animation Here
}

void KeyFrame::interpolate(const KeyFrame& from,
	                        const KeyFrame& to,
	                        float tau,
	                        KeyFrame& target) {
	target.world_pos = glm::mix(from.world_pos, to.world_pos, tau);
	for (int i = 0; i < to.rel_rot.size(); i++) {
		glm::fquat result = identity_quat;
		if (to.rel_rot.at(i) != result) {
			result = glm::slerp(from.rel_rot.at(i), to.rel_rot.at(i), tau);
		}
		target.rel_rot.push_back(result);
	}
	// std::cout << tau << " " << target.world_pos.x << " " << target.world_pos.y << " " << target.world_pos.z << std::endl;
}



const Configuration*
Mesh::getCurrentQ() const
{
	return &currentQ_;
}

Configuration*
Mesh::getCurrentQ_()
{
	return &currentQ_;
}

void Mesh::setRots() {
	auto q = getCurrentQ_();
	// for (int i = 0; i < skeleton.joints.size(); i++) {
	// 	auto& qrot = q->rot.at(i);
	// 	qrot = skeleton.joints.at(i).orientation;
	// }
}