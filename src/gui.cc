#include "gui.h"
#include "config.h"
#include <jpegio.h>
#include "bone_geometry.h"
#include <iostream>
#include <algorithm>
#include <debuggl.h>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>



namespace {
	// FIXME: Implement a function that performs proper
	//        ray-cylinder intersection detection
	// TIPS: The implement is provided by the ray-tracer starter code.
}

GUI::GUI(GLFWwindow* window, int view_width, int view_height, int preview_height)
	:window_(window), preview_height_(preview_height)
{
	glfwSetWindowUserPointer(window_, this);
	glfwSetKeyCallback(window_, KeyCallback);
	glfwSetCursorPosCallback(window_, MousePosCallback);
	glfwSetMouseButtonCallback(window_, MouseButtonCallback);
	glfwSetScrollCallback(window_, MouseScrollCallback);

	glfwGetWindowSize(window_, &window_width_, &window_height_);
	if (view_width < 0 || view_height < 0) {
		view_width_ = window_width_;
		view_height_ = window_height_;
	} else {
		view_width_ = view_width;
		view_height_ = view_height;
	}
	float aspect_ = static_cast<float>(view_width_) / view_height_;
	projection_matrix_ = glm::perspective((float)(kFov * (M_PI / 180.0f)), aspect_, kNear, kFar);
}

GUI::~GUI()
{
}

void GUI::assignMesh(Mesh* mesh)
{
	mesh_ = mesh;
	center_ = mesh_->getCenter();
}

void GUI::keyCallback(int key, int scancode, int action, int mods)
{
#if 0
	if (action != 2)
		std::cerr << "Key: " << key << " action: " << action << std::endl;
#endif
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window_, GL_TRUE);
		return ;
	}
	if (key == GLFW_KEY_J && action == GLFW_RELEASE) {
		//FIXME save out a screenshot using SaveJPEG
		std::string filename = "./aoeu.jpg";
		std::string file = "aoeu.jpg";
		
		unsigned char* pixels = (unsigned char*) malloc(3*view_width_*view_height_);
		CHECK_GL_ERROR(glReadPixels(0, 0, view_width_, view_height_, GL_RGB, GL_UNSIGNED_BYTE, pixels));
		// for (int i = 0; i < 3*view_width_*view_height_; i++) {
		// 	std::cerr << int(pixels[i]) << " ";
		// }
		bool result = SaveJPEG(filename, view_width_, view_height_, pixels);
		if (result) {
			std::cout << "Encoding and saving to file '" << file << "'" << std::endl;
		}
		else {
			std::cout << "fail" << std::endl;
		}
	}
	if (key == GLFW_KEY_S && (mods & GLFW_MOD_CONTROL)) {
		if (action == GLFW_RELEASE)
			mesh_->saveAnimationTo("animation.json");
			std::cout << "Success!" << std::endl;
		return ;
	}

	if (key == GLFW_KEY_O && (mods & GLFW_MOD_CONTROL)) {
		if (action == GLFW_RELEASE)
			mesh_->loadAnimationFrom("animation.json");
			std::cout << "Success!" << std::endl;
			pose_changed_ = true;
		return ;
	}
	if (key == GLFW_KEY_S && (mods & GLFW_MOD_ALT)) {
		if (action == GLFW_RELEASE)
			save_file_ = true;
			mesh_->saveAnimationTo("aoeu.json");
			std::cout << "Success!" << std::endl;
		return ;
	}

	if (mods == 0 && captureWASDUPDOWN(key, action))
		return ;
	if (key == GLFW_KEY_LEFT || key == GLFW_KEY_RIGHT) {
		float roll_speed;
		if (key == GLFW_KEY_RIGHT)
			roll_speed = -roll_speed_;
		else
			roll_speed = roll_speed_;
		// FIXME: actually roll the bone here
		int bone = getCurrentBone();
		Bone& b = mesh_->skeleton.bones.at(bone);
		glm::vec3 axis = b.cylinder.v_;
		glm::fquat r = glm::angleAxis(roll_speed, axis);
		auto& rot = mesh_->skeleton.joints.at(bone).rel_orientation;
		rot = r * rot;
		// rot = mesh_->skeleton.joints.at(bone).orientation;
		// rot = r * rot;
		// b.cylinder.rollCylinder(r);
		pose_changed_ = true;
		transformJoint(bone);
		for (int i = 0; i < mesh_->skeleton.joints.size(); i++) {
			if (mesh_->skeleton.joints.at(i).parent_index == -1) {
				transformJoint(i);
			}
		}
	} else if (key == GLFW_KEY_C && action != GLFW_RELEASE) {
		fps_mode_ = !fps_mode_;
	} else if (key == GLFW_KEY_LEFT_BRACKET && action == GLFW_RELEASE) {
		current_bone_--;
		current_bone_ += mesh_->getNumberOfBones();
		current_bone_ %= mesh_->getNumberOfBones();
	} else if (key == GLFW_KEY_RIGHT_BRACKET && action == GLFW_RELEASE) {
		current_bone_++;
		current_bone_ += mesh_->getNumberOfBones();
		current_bone_ %= mesh_->getNumberOfBones();
	} else if (key == GLFW_KEY_T && action != GLFW_RELEASE) {
		transparent_ = !transparent_;
	} else if (key == GLFW_KEY_O) {
		glm::vec3 t = glm::vec3(0, 0, -move_speed_);
		rotate_joint(t);
	} else if (key == GLFW_KEY_L) {
		glm::vec3 t = glm::vec3(0, 0, move_speed_);
		rotate_joint(t);
	} else if (key == GLFW_KEY_SEMICOLON) {
		glm::vec3 t = glm::vec3(move_speed_, 0, 0);
		rotate_joint(t);
	} else if (key == GLFW_KEY_K) {
		glm::vec3 t = glm::vec3(-move_speed_, 0, 0);
		rotate_joint(t);
	} else if (key == GLFW_KEY_SPACE) {
		glm::vec3 t = glm::vec3(0, move_speed_, 0);
		rotate_joint(t);
	} else if (key == GLFW_KEY_TAB) {
		glm::vec3 t = glm::vec3(0, -move_speed_, 0);
		rotate_joint(t);
	} 

	// FIXME: implement other controls here.
	if (key == GLFW_KEY_F && action == GLFW_RELEASE) {
		KeyFrame kf;
		kf.world_pos = mesh_->skeleton.world_joint.position;
		for (int i = 0; i < mesh_->skeleton.joints.size(); i++) {
			Joint j = mesh_->skeleton.joints.at(i);
			kf.rel_rot.push_back(j.rel_orientation);
		}
		mesh_->keyframes.push_back(kf);
		if (mesh_->keyframes.size() == 1) {
			mesh_->current = kf;
			mesh_->next = kf;
		}
		keyframe = true;
	}
	if (key == GLFW_KEY_P && action == GLFW_RELEASE) {
		if (!play_) {
			if (reset_) {
				start = std::chrono::high_resolution_clock::now();
				reset_ = false;
			} else {
				auto difference = std::chrono::high_resolution_clock::now() - current;
				start = start + difference;
				current = current + difference;
			}
			play_ = true;
		}
		else {
			current = std::chrono::high_resolution_clock::now();
			play_ = false;

		}
	}
	if (key == GLFW_KEY_R && action == GLFW_RELEASE) {
		if (!play_) {
			reset_ = true;
		} else {
			start = std::chrono::high_resolution_clock::now();
			current = std::chrono::high_resolution_clock::now();
		}
		mesh_->keyframe_index = 0;
	}

}

void GUI::transformJoint(int i) {
	Joint& j = mesh_->skeleton.joints.at(i);
	Bone& b = mesh_->skeleton.bones.at(i);
	glm::mat4 d_i;
	if (j.parent_index != -1) {
		Joint& p_j = mesh_->skeleton.joints.at(j.parent_index);
		d_i = mesh_->skeleton.getRecursiveD_i(j);
		glm::vec4 map = d_i * glm::vec4(mesh_->skeleton.world_joint.init_position, 1.0f);
		j.position = glm::vec3(map.x, map.y, map.z);
		glm::mat3 rotation_matrix = glm::mat3(d_i[0][0], d_i[0][1], d_i[0][2],
											  d_i[1][0], d_i[1][1], d_i[1][2],
											  d_i[2][0], d_i[2][1], d_i[2][2]);
		j.orientation = glm::quat_cast(rotation_matrix);
		// j.orientation = p_j.orientation * j.orientation;
		b.moveBone(p_j, j);
	} else {
		Joint& p_j = mesh_->skeleton.world_joint;
		d_i = mesh_->skeleton.getRecursiveD_i(j);
		glm::vec4 map = d_i * glm::vec4(mesh_->skeleton.world_joint.init_position, 1.0f);
		j.position = glm::vec3(map.x, map.y, map.z);
		// j.orientation = p_j.orientation * j.orientation;
		glm::mat3 rotation_matrix = glm::mat3(d_i[0][0], d_i[0][1], d_i[0][2],
											  d_i[1][0], d_i[1][1], d_i[1][2],
											  d_i[2][0], d_i[2][1], d_i[2][2]);
		j.orientation = glm::quat_cast(rotation_matrix);
		b.moveBone(p_j, j);
	}
	
	
	for (int c : j.children) {
		transformJoint(c);
	}
}

bool GUI::castRay(double i, double j)
{
	bool result = false;
	double x = double(i)/double(view_width_);
	double y = double(j)/double(view_height_);

	double a = (view_width_ * 1.0f) / (view_height_ * 1.0f);
	//std::cerr << a << " " << view_width_ / view_height_ << std::endl ;
	double theta = double(kFov) * kPi/180;
	double h = 2 * double(kNear) * glm::tan(theta / 2);
	double l = 2 * a * double(kNear) * glm::tan(theta / 2);
	//std::cerr << view_width_ << " " << view_height_ << " " << view_width_ / view_height_ << " " << a << " " << h << " " << l << std::endl;
	x *= l;
	y *= h;
	x -= l / 2;
	y -= h / 2;
	// std::cerr << glm::sin(180) << " " << glm::sin(/2) << std::endl;
	//x -= 0.5;
	//y -= 0.5;
	glm::vec3 direction = glm::normalize(glm::vec3(x, y, kNear));
	glm::mat3 rotation = glm::mat3(tangent_.x, tangent_.y, tangent_.z,
		up_.x, up_.y, up_.z,
		look_.x, look_.y, look_.z);
	direction = glm::normalize(rotation * direction);
	//glm::vec3 direction = glm::normalize()
	r.setPosition(eye_);
	r.setDirection(direction);
	std::cerr << direction.x << " " << direction.y << " " << direction.z << std::endl;
	direction = r.getDirection();
	glm::vec3 eye = r.getPosition();
	int bone = mesh_->intersect(r);
	result = setCurrentBone(bone);
	//  std::cerr << i << " " << j << std::endl;
	return result;
}



void GUI::rotate_joint(glm::vec3& t) {
	glm::vec3& root = mesh_->skeleton.joints.at(0).position;
	glm::vec3& world = mesh_->skeleton.world_joint.position;
	glm::fquat rotation = mesh_->skeleton.world_joint.orientation;
	if (rotation.w < 1.0 - RAY_EPSILON) 
	t = glm::rotate(rotation, t); // rotated movement direction
	mesh_->skeleton.world_joint.position += t;
	glm::vec3& world_pos = mesh_->skeleton.world_joint.position;
	glm::vec4 w4 = mesh_->skeleton.world_joint.get_di() * glm::vec4(mesh_->skeleton.world_joint.init_position, 1.0);
	mesh_->skeleton.world_joint.position = glm::vec3(w4.x, w4.y, w4.z);
	mesh_->skeleton.world_joint.rel_position = mesh_->skeleton.world_joint.position;
	pose_changed_ = true;
	transformJoint(0);
	for (int i = 0; i < mesh_->skeleton.joints.size(); i++) {
		if (mesh_->skeleton.joints.at(i).parent_index == -1) {
			transformJoint(i);
		}
	}
}

glm::vec2 GUI::getPixel(glm::vec3& rotation_origin) {
	glm::vec3 direction = glm::normalize(rotation_origin - eye_);
	glm::fquat rotation = glm::rotation(look_, glm::vec3(0, 0, -1));
	direction = glm::rotate(rotation, direction);

	double a = (view_width_ * 1.0f) / (view_height_ * 1.0f);
	// std::cerr << a << " " << view_width_ / view_hight_ << std::endl ;
	// std::cerr << direction.x << " " << direction.y << " " << direction.z << std::endl;
	double theta = double(kFov) * kPi/180;
	double h = 2 * double(kNear) * glm::tan(theta / 2);
	double l = 2 * a * double(kNear) * glm::tan(theta / 2);
	double h2 = h / 2;
	double l2 = l / 2;
	double hypotenuse = glm::sqrt(kNear*kNear - h2*h2);
	double height = glm::sqrt(hypotenuse*hypotenuse - l2*l2);
	float m = glm::abs(float(height) / direction.z);
	// std::cerr << theta << " " << h << " " << l << " " << h2 << " " << l2 << " " << hypotenuse << " " << height << " " << m << std::endl;
	direction = m * direction;
	// std::cerr << direction.x << " " << direction.y << " " << direction.z << std::endl;
	direction.x += l2;
	direction.y += h2;
	// std::cerr << direction.x << " " << direction.y << " " << direction.z << std::endl;
	// std::cerr << direction.x * l / (view_width_ * 1.0f) << std::endl;;
	int x = (direction.x*1.0f) * (view_width_*1.0f)/(l*1.0f);
	int y = (direction.y*1.0f) * (view_height_*1.0f)/(h*1.0f);
	y = view_height_ - y;
	// std::cerr << x << " " << y << std::endl;
	return glm::vec2(x, y);
}

void GUI::mousePosCallback(double mouse_x, double mouse_y)
{
	last_x_ = current_x_;
	last_y_ = current_y_;
	current_x_ = mouse_x;
	current_y_ = window_height_ - mouse_y;
	float delta_x = current_x_ - last_x_;
	float delta_y = current_y_ - last_y_;
	if (sqrt(delta_x * delta_x + delta_y * delta_y) < 1e-15)
		return;
	if (mouse_x > view_width_)
		return ;
	glm::vec3 mouse_direction = glm::normalize(glm::vec3(delta_x, delta_y, 0.0f));
	glm::vec2 mouse_start = glm::vec2(last_x_, last_y_);
	glm::vec2 mouse_end = glm::vec2(current_x_, current_y_);
	glm::uvec4 viewport = glm::uvec4(0, 0, view_width_, view_height_);

	bool drag_camera = drag_state_ && current_button_ == GLFW_MOUSE_BUTTON_RIGHT;
	bool drag_bone = drag_state_ && current_button_ == GLFW_MOUSE_BUTTON_LEFT;

	if (drag_camera) {
		glm::vec3 axis = glm::normalize(
				orientation_ *
				glm::vec3(mouse_direction.y, -mouse_direction.x, 0.0f)
				);
		orientation_ =
			glm::mat3(glm::rotate(rotation_speed_, axis) * glm::mat4(orientation_));
		tangent_ = glm::column(orientation_, 0);
		up_ = glm::column(orientation_, 1);
		look_ = glm::column(orientation_, 2);
	} else if (drag_bone && current_bone_ != -1) {
		// FIXME: Handle bone rotation
		Bone& b = mesh_->skeleton.bones.at(current_bone_);

		glm::vec2 pjoint = getPixel(b.p);
		glm::vec2 cjoint = getPixel(b.c);
		glm::vec3 bone = glm::vec3(cjoint.x - pjoint.x, cjoint.y - pjoint.y, 0.0);
		glm::vec3 drag = glm::vec3(delta_x, -delta_y, 0);
		glm::mat3 rotation = glm::inverse(glm::mat3(bone.y, -bone.x, 0.0,
									   bone.x, bone.y, 0.0,
									   0, 0, 1));
		bone = rotation * bone;
		drag = rotation * drag;
		// std::cerr << pjoint.x << ' ' << pjoint.y << ' ' << cjoint.x << ' ' << cjoint.y << std::endl;
		
		float theta = 0;
		glm::vec3 axis = look_;
		if (drag.x < 0) {
			axis = look_;
			theta = 1/(kPi * 25);
		} else if (drag.x > 0) {
			axis = -look_;
			theta = 1/(kPi * 25);
		}

		glm::fquat rot = glm::angleAxis(theta, axis);
		auto& r = mesh_->skeleton.joints.at(current_bone_).rel_orientation;
		r = rot*r;
		pose_changed_ = true;

		transformJoint(current_bone_);
		for (int i = 0; i < mesh_->skeleton.joints.size(); i++) {
			if (mesh_->skeleton.joints.at(i).parent_index == -1) {
				transformJoint(i);
			}
		}
		// for (int i = 0; i < mesh_->skeleton.joints.size(); i++) {
		// 	if (mesh_->skeleton.joints.at(i).parent_index == -1 && i != current_bone_) {
		// 		r = mesh_->skeleton.joints.at(i).rel_orientation;
		// 		r = rot*r;
		// 		pose_changed_ = true;
		// 		transformJoint(i);
		// 	}
		// }
		return ;
	}

	// FIXME: highlight bones that have been moused over
	bool intersects = castRay(current_x_, current_y_);
}

void GUI::mouseButtonCallback(int button, int action, int mods)
{
	if (current_x_ <= view_width_) {
		drag_state_ = (action == GLFW_PRESS);
		current_button_ = button;
		return ;
	}
	// FIXME: Key Frame Selection
}

void GUI::mouseScrollCallback(double dx, double dy)
{
	if (current_x_ < view_width_)
		return;
	// FIXME: Mouse Scrolling
}

void GUI::updateMatrices()
{
	// Compute our view, and projection matrices.
	if (fps_mode_)
		center_ = eye_ + camera_distance_ * look_;
	else
		eye_ = center_ - camera_distance_ * look_;

	view_matrix_ = glm::lookAt(eye_, center_, up_);
	light_position_ = glm::vec4(eye_, 1.0f);

	aspect_ = static_cast<float>(view_width_) / view_height_;
	projection_matrix_ =
		glm::perspective((float)(kFov * (M_PI / 180.0f)), aspect_, kNear, kFar);
	model_matrix_ = glm::mat4(1.0f);
}

MatrixPointers GUI::getMatrixPointers() const
{
	MatrixPointers ret;
	ret.projection = &projection_matrix_;
	ret.model= &model_matrix_;
	ret.view = &view_matrix_;
	return ret;
}

bool GUI::setCurrentBone(int i)
{
	if (i < 0 || i >= mesh_->getNumberOfBones())
		return false;
	current_bone_ = i;
	return true;
}

float GUI::getCurrentPlayTime() const
{
	if (play_) {
		std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> duration = now - start;
		return duration.count();
	} else {
		std::chrono::duration<double> duration = current - start;
		return duration.count();
	}
	return 0.0f;
}


bool GUI::captureWASDUPDOWN(int key, int action)
{
	if (key == GLFW_KEY_W) {
		if (fps_mode_)
			eye_ += zoom_speed_ * look_;
		else
			camera_distance_ -= zoom_speed_;
		return true;
	} else if (key == GLFW_KEY_S) {
		if (fps_mode_)
			eye_ -= zoom_speed_ * look_;
		else
			camera_distance_ += zoom_speed_;
		return true;
	} else if (key == GLFW_KEY_A) {
		if (fps_mode_)
			eye_ -= pan_speed_ * tangent_;
		else
			center_ -= pan_speed_ * tangent_;
		return true;
	} else if (key == GLFW_KEY_D) {
		if (fps_mode_)
			eye_ += pan_speed_ * tangent_;
		else
			center_ += pan_speed_ * tangent_;
		return true;
	} else if (key == GLFW_KEY_DOWN) {
		if (fps_mode_)
			eye_ -= pan_speed_ * up_;
		else
			center_ -= pan_speed_ * up_;
		return true;
	} else if (key == GLFW_KEY_UP) {
		if (fps_mode_)
			eye_ += pan_speed_ * up_;
		else
			center_ += pan_speed_ * up_;
		return true;
	}
	return false;
}


// Delegrate to the actual GUI object.
void GUI::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	GUI* gui = (GUI*)glfwGetWindowUserPointer(window);
	gui->keyCallback(key, scancode, action, mods);
}

void GUI::MousePosCallback(GLFWwindow* window, double mouse_x, double mouse_y)
{
	GUI* gui = (GUI*)glfwGetWindowUserPointer(window);
	gui->mousePosCallback(mouse_x, mouse_y);
}

void GUI::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	GUI* gui = (GUI*)glfwGetWindowUserPointer(window);
	gui->mouseButtonCallback(button, action, mods);
}

void GUI::MouseScrollCallback(GLFWwindow* window, double dx, double dy)
{
	GUI* gui = (GUI*)glfwGetWindowUserPointer(window);
	gui->mouseScrollCallback(dx, dy);
}
