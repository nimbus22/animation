#ifndef CONFIG_H
#define CONFIG_H

/*
 * Global variables go here.
 */

const float kCylinderRadius = 0.25;
const int kMaxBones = 128;
/*
 * Extra credit: what would happen if you set kNear to 1e-5? How to solve it?
 */
const float kNear = 0.1;
const float kFar = 1000.0f;
const float kFov = 45.0f;
const float kPi = 3.1415926535897932384626433832795;

// Floor info.
const float kFloorEps = 0.5 * (0.025 + 0.0175);
const float kFloorXMin = -100.0f;
const float kFloorXMax = 100.0f;
const float kFloorZMin = -100.0f;
const float kFloorZMax = 100.0f;
const float kFloorY = -0.75617 - kFloorEps;

const float kTextBoxHeight = 360;
const float kInputBoxHeight = 40;
const float kWindowWidth = 1280;
const float kWindowHeight = 720;

const float kScrollSpeed = 64.0f;

#endif