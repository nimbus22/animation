#ifndef PROCEDURE_GEOMETRY_H
#define PROCEDURE_GEOMETRY_H

#include <vector>
#include <glm/glm.hpp>

struct LineMesh;

void create_floor(std::vector<glm::vec4>& floor_vertices, std::vector<glm::uvec3>& floor_faces);
void create_text_box(std::vector<glm::vec4>& text_box_vertices, std::vector<glm::uvec3>& text_box_faces, 
		std::vector<glm::vec4>& input_box_vertices, std::vector<glm::uvec3>& input_box_faces);
void create_texture_quad(std::vector<glm::vec4> & texture_quad_vertices, std::vector<glm::uvec3>& texture_quad_faces);
void create_bone_mesh(LineMesh& bone_mesh);
void create_cylinder_mesh(LineMesh& cylinder_mesh);
void create_axes_mesh(LineMesh& axes_mesh);

#endif
