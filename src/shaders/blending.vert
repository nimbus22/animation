R"zzz(
#version 330 core
uniform vec4 light_position;
uniform vec3 camera_position;

uniform vec3 joint_trans[128];
uniform vec4 joint_rot[128];
uniform mat4 tx_d[128];
uniform mat4 tx_u[128];

in int jid0;
in int jid1;
in float w0;
in vec3 vector_from_joint0;
in vec3 vector_from_joint1;
in vec4 normal;
in vec2 uv;
in vec4 vert;

out vec4 vs_light_direction;
out vec4 vs_normal;
out vec2 vs_uv;
out vec4 vs_camera_direction;

vec3 qtransform(vec4 q, vec3 v) {
	return v + 2.0 * cross(cross(v, q.xyz) - q.w*v, q.xyz);
}

void main() {
	// FIXME: Implement linear skinning here
	vec4 position;
	if (jid1 != -1) {
		position = vec4(w0 * tx_d[jid0] * (tx_u[jid0]) * vert) + vec4((1-w0) * tx_d[jid1] * (tx_u[jid1]) * vert);
		position = vec4(position.x, position.y, position.z, 1.0);
	} else {
		// vec4 r = tx_u[jid0] * vert;
		// mat4 d = tx_d[jid0];
		// mat4 rot = mat4(d[0][0], d[0][1], d[0][2], 0,
		// 				d[1][0], d[1][1], d[1][2], 0,
		// 				d[2][0], d[2][1], d[2][2], 0,
		// 				0, 0, 0, 1);
		// vec3 qr = qtransform(joint_rot[jid0], vec3(r.x, r.y, r.z));
		// vec3 qrj = vec3(qr.x, qr.y, qr.z) + joint_trans[jid0];
		position = tx_d[jid0] * tx_u[jid0] * vert;
		position = vec4(position.x, position.y, position.z, 1.0);
	}

	
	
	gl_Position = position;
	vs_normal = normal;
	vs_light_direction = light_position - gl_Position;
	vs_camera_direction = vec4(camera_position, 1.0) - gl_Position;
	vs_uv = uv;
}
)zzz"
