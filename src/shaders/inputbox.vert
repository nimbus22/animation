R"zzz(#version 330 core
layout (location = 0) in vec4 vertex_position; // <vec2 pos, vec2 tex>

uniform mat4 text_projection;

void main()
{
    gl_Position = text_projection * vec4(vertex_position.xy, 0.0, 1.0);
})zzz"
