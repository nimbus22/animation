# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/nimbus/Documents/animation/src/animation_loader_saver.cc" "/home/nimbus/Documents/animation/build/src/CMakeFiles/animation.dir/animation_loader_saver.cc.o"
  "/home/nimbus/Documents/animation/src/bone_geometry.cc" "/home/nimbus/Documents/animation/build/src/CMakeFiles/animation.dir/bone_geometry.cc.o"
  "/home/nimbus/Documents/animation/src/gui.cc" "/home/nimbus/Documents/animation/build/src/CMakeFiles/animation.dir/gui.cc.o"
  "/home/nimbus/Documents/animation/src/main.cc" "/home/nimbus/Documents/animation/build/src/CMakeFiles/animation.dir/main.cc.o"
  "/home/nimbus/Documents/animation/src/procedure_geometry.cc" "/home/nimbus/Documents/animation/build/src/CMakeFiles/animation.dir/procedure_geometry.cc.o"
  "/home/nimbus/Documents/animation/src/ray.cpp" "/home/nimbus/Documents/animation/build/src/CMakeFiles/animation.dir/ray.cpp.o"
  "/home/nimbus/Documents/animation/src/render_pass.cc" "/home/nimbus/Documents/animation/build/src/CMakeFiles/animation.dir/render_pass.cc.o"
  "/home/nimbus/Documents/animation/src/shader_uniform.cc" "/home/nimbus/Documents/animation/build/src/CMakeFiles/animation.dir/shader_uniform.cc.o"
  "/home/nimbus/Documents/animation/src/texture_to_render.cc" "/home/nimbus/Documents/animation/build/src/CMakeFiles/animation.dir/texture_to_render.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLM_ENABLE_EXPERIMENTAL"
  "GLM_FORCE_RADIANS=1"
  "GLM_FORCE_SIZE_FUNC=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../third-party/glm"
  "/usr/include/eigen3"
  "/usr/local/include"
  "/opt/local/include"
  "../lib"
  "../lib/utgraphicsutil"
  "../lib/pmdreader"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/nimbus/Documents/animation/build/CMakeFiles/utgraphicsutil.dir/DependInfo.cmake"
  "/home/nimbus/Documents/animation/build/CMakeFiles/pmdreader.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
